﻿<%@ Page Language="C#" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">
    void Login_Authenticate(object sender, AuthenticateEventArgs e)
    {
        LoginDataSource.SelectCommand = "SELECT * FROM Login WHERE username = '" + Login.UserName +
         "' AND password = '" + Login.Password + "'";

        LoginDataSource.Select(DataSourceSelectArguments.Empty);
    }
    private void CheckLogin(object sender, SqlDataSourceStatusEventArgs e)
    {
        if (e.AffectedRows > 0)
        {
            FormsAuthentication.SetAuthCookie(Login.UserName, false);
           FormsAuthentication.RedirectFromLoginPage(Login.UserName, true);
            
            //Response.Redirect("Ass2/index.aspx");
        }
        else
        {
            Login.FailureText = "Invalid Login";
        }
    }
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Login Page</title>
    <link href="~/Ass2/Styles/main.css" rel="stylesheet" type="text/css" />
</head>
<body id="loginPage">
    <form id="form1" runat="server">
    <div>
        <asp:AccessDataSource ID="LoginDataSource" runat="server" 
        DataFile="~/App_Data/Login.accdb" OnSelected="CheckLogin"></asp:AccessDataSource>
        <h1 id="loginHeader">Log In</h1>
        <hr />
        <br /><br />
        <div id="loginInputs">
            <asp:Login ID="Login" runat="server" OnAuthenticate="Login_Authenticate" 
                DisplayRememberMe="false" TitleText="Please enter your username and password." >
                <LoginButtonStyle CssClass="button" Width="80px" />
            </asp:Login>
            <asp:ValidationSummary id="vlSummary1" Visible="true" CssClass="vldSummary"
                runat="server" ValidationGroup="Login" 
                HeaderText="Please correct the following errors:" ForeColor="Red" />
        </div>
    </div>
    <br /><br /><br /><br /><br />
    <a class="blueImgBtn" href="Ass2/DisplayCode.aspx?filename=~/Login.aspx&filename2=~/Web.config" target="_blank">
        <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/codebuttonlogin.jpg" />
     </a>
    </form>
</body>
</html>
