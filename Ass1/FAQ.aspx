﻿<%@ Page Title="FAQ" Language="C#" MasterPageFile="~/Phonehub.master" StylesheetTheme="SkinFile" %>

<script runat="server">

</script>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:SiteMapPath ID="SiteMapPath1" runat="server" SiteMapProvider="MyAss1SiteMapProvider">
    </asp:SiteMapPath>
    <h1>FAQ</h1>
    <hr />
    <div id="faqs">
        <dl>
            <dt>What is Phonehub?</dt>
            <dd>
                Phonehub is an about showing and experiencing exciting news, products, stories and opinions
                in technology. We do reviews about consumer electronic, post and comment cutting edge technologies
                and share opinions with our readers. Our goal is to do our best to share the love of technology
                with our readers.
            </dd>
            <hr />
            <dt>What products will you review?</dt>
            <dd>
                Basically we review products which are the latest, the hottest and the most popular.
                However, sometimes we also do reviews that may be old fashioned or unpopular but many people
                are interested in.
            </dd>
            <hr /> 
            <dt>How do you choose what stories to cover?</dt>
            <dd>
                We are interested in any breaking news happening in technology, this is not limited to
                electronic products, there are also things about games, technology companies, interface
                design, geeky things and so on.
            </dd>
            <hr /> 
            <dt>Do you sell any product?</dt>
            <dd>No, we don't. All the products that Phonehub reviewing will be returned to the companies.</dd>
            <hr /> 
            <dt>How can I get in touch with Phonehub?</dt>
            <dd>
                You can follow us on facebook, twitter or google+ to share opinions and make comments, or send 
                us mails and emails. You may also email our support staff.
            </dd>
            <hr />
            <dt>Can you review the product that I am interested in?</dt>
            <dd>
                We only review products that are the latest in the market or more popular, if you have a
                favourite product that you want us to review you can email us and we will decide whether
                to do it or not.
            </dd>
            <hr />
            <dt>Do companies pay you to do reviews?</dt>
            <dd>
                Phonehub has a strict policy against keeping free products. All the products we reviewed will
                be returned to the companies and we will never accept free products in the future.
            </dd>
        </dl>
    </div>
</asp:Content>

