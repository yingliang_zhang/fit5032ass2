﻿<%@ Page Title="About Us" Language="C#" MasterPageFile="~/Phonehub.master" StylesheetTheme="SkinFile" %>

<script runat="server">

</script>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:SiteMapPath ID="SiteMapPath1" runat="server" SiteMapProvider="MyAss1SiteMapProvider">
    </asp:SiteMapPath>
    <h1>About Us</h1>
    <hr />
    <img id="aboutLogo" src="../Images/phonehublogo.png" alt="PHONEHUB" />
    <div id="aboutText">
    <p>
        Phonehub is a news, reviews, ideas and opinions posting and sharing website, we write articles and
        reviews about cutting edge technology that related to consumer electronics, especially mobile phones,
        mobile operating systems, cellphone companies and the science they're built upon. The content on Phonehub
        is mainly about mobile phone unboxing, guides and reviews. In order to make our content more interesting
        and alive, we make frequent use of video to supplement our text content. Our articles are written
        by professional technology blog writers, observers and tech-enthusiasts.
    </p>
    <p>
        The core of Phonehub is about experiencing the beauty and joy in technology, which includes sharing
        views and opinions with our readers.We hope our visitors can find their love for technology and lead
        the tech filled lifesytle that they want through the reviews, stories and ideas on Phonehub. We also
        hope that we can provide customers practical information and advices to help them in finding the
        technology and products that best suits their needs.
    </p>
    </div>
</asp:Content>

