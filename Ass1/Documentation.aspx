﻿<%@ Page Title="Documentation" Language="C#" MasterPageFile="~/Phonehub.master" StylesheetTheme="SkinFile" %>

<script runat="server">

</script>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:SiteMapPath ID="SiteMapPath1" runat="server" SiteMapProvider="MyAss1SiteMapProvider">
    </asp:SiteMapPath>
    <h1>Documentation</h1>
    <hr />
    <ul>
        <li><b>Name:</b> Yingliang Zhang</li>
        <li><b>StudentID:</b> 24047392</li>
        <li><b>Unit Name:</b> FIT5032 Internet Applications Development</li>
        <li><b>Unit Provider:</b> Monash University, The Caulfield School of Information Technology</li>
        <li><b>Assignment Number:</b> 1</li>
        <li><b>Submission Date:</b> </li>
        <li><b>Tutor:</b> Ben Bina</li>
        <li><b>Email:</b> <a href="mailto:yzha401@student.monash.edu">Author</a></li>
        <li><b><a href="https://walkabout.infotech.monash.edu.au/walkabout/fit5032/assignments/assignment1.aspx?unit=fit5032">Assignment Specification</a></b></li>
    </ul>
</asp:Content>

