﻿<%@ Page Title="Phonehub" Language="C#" MasterPageFile="~/Phonehub.master" StylesheetTheme="SkinFile" %>
<script runat="server">

</script>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:SiteMapPath ID="SiteMapPath1" runat="server" 
    SiteMapProvider="MyAss1SiteMapProvider">
    </asp:SiteMapPath>
    <h1>
        Latest phone reviews at Phonehub!
    </h1>
    <hr />
    <div id="banner">
        <asp:AdRotator ID="AdRotator1" runat="server" AdvertisementFile="BannerAds.xml" />
    </div>
    <div id="phoneArticles">
        <div id="article1">
            <h2>Galaxy Golden</h2>
            <p>
                Samsung just announces another dualscreen flip-phone: the Galaxy Golden.
                The Golden sports dual 3.7-inch Super AMOLED screens, Android 4.2 Jelly Bean, a 1.7GHz
                dual-core processor and an 8MP camera.
            </p>
        </div>
        <div id="article2">
            <h2>2,560X1,440 LCD for smartphones</h2>
            <p>
                LG Display has just laid claim to the world's first 2,560X1,440 smartphone
                display, this display has higher ppi than retina display, which is 538ppi.
            </p>
        </div>
        <div id="article3">
            <h2>Moto X review</h2>
            <p>
                Google Moto X is a special member of the phone market.The personalization way it 
                chooses,the user-friendly enhancements it focuses, its steller battery life and 
                crowdsourced design all show that Moto X is a tough rival to its competitors.
            </p>
        </div>
    </div>
    <div class="clear">
        <p></p>
    </div>
    <hr />
    <div id="bottomInfo">
        <p><b>Contact Information:</b></p>
        <ul>
            <li><a href="http://users.monash.edu.au/~sgrose/msh/disclaimer.htm">Monash course disclaimer</a></li>
            <li><b>E-mail:</b>&nbsp;<a href="mailto:yzha401@student.monash.edu">Author</a></li>
            <li><b>E-mail:</b>&nbsp;<a href="mailto:yzha401@student.monash.edu">Webmaster</a></li>
        </ul>
        <p>
            <b>Copyright Notice:</b>
            All materials contained on this website are copyrighted by Yingliang Zhang except images used 
            for home page banners. No use of the materials on this website without express permission 
            from Yinglinag Zhang. 
        </p>
        <p><b>Acknowledgements:</b> The following source images used for banners are free to use for
        nonbusiness purposes, all of these images are under Copyrignt &copy; 2006-2013 ZCOOL lisence,
        copyright info can be found at 
        <a href="http://www.zcool.com.cn/service/copyright.html">http://www.zcool.com.cn/service/copyright.html</a>, section 6.</p>
        <ul>
            <li>Home page banner 1: <a href="http://www.zcool.com.cn/gfx/ZMjY2NDU2.html">Mi-Phone, </a>
                    <a href="http://www.zcool.com.cn/gfx/ZMjczMjE2.html">Icons</a></li>
            <li>Home page banner 2: <a href="http://www.zcool.com.cn/gfx/ZMjg3MDQ4.html">HTC One Mini</a></li>
            <li>Home page banner 3: <a href="http://www.zcool.com.cn/gfx/ZMjc0OTQ0.html">iPhone5</a></li>
            </ul>
            <a class="blueImgBtn" href="DisplayCode.aspx?filename=~/Ass2/index.aspx&filename2=~/Ass2/BannerAds.xml" target="_blank">
                <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/codebuttonAddrotator.jpg" />
            </a>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <a class="blueImgBtn" href="DisplayCode.aspx" target="_blank">
                <asp:Image ID="Image2" runat="server" ImageUrl="~/Images/codebuttonMasterPage.jpg" />
            </a>
    </div>
    
</asp:Content>

