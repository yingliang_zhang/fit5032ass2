﻿<%@ Page Language="C#" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Import Namespace="System.IO" %> 
<script runat="server">
    void Page_Load()
    {
        if (Request.QueryString["filename"] != null && Request.QueryString["filename2"] != null && Request.QueryString["filename3"] != null)
        {
            FileName.Text = Request.QueryString["filename"] + Request.QueryString["filename2"] + Request.QueryString["filename3"];
            string filePath = Server.MapPath(Request.QueryString["filename"]);
            string filename2Path = Server.MapPath(Request.QueryString["filename2"]);
            string filename3Path = Server.MapPath(Request.QueryString["filename3"]);
            Code.Text = ReadFile(filePath) + ReadFile(filename2Path) + ReadFile(filename3Path);
        }
        else if (Request.QueryString["filename"] != null && Request.QueryString["filename2"] != null && Request.QueryString["filename3"] == null)
        {
            FileName.Text = Request.QueryString["filename"] + Request.QueryString["filename2"];
            string filePath = Server.MapPath(Request.QueryString["filename"]);
            string filename2Path = Server.MapPath(Request.QueryString["filename2"]);
            Code.Text = ReadFile(filePath) + ReadFile(filename2Path);
        }
        else if (Request.QueryString["filename"] != null && Request.QueryString["filename2"] == null && Request.QueryString["filename3"] == null)
        {
            FileName.Text = Request.QueryString["filename"];
            string filePath = Server.MapPath(Request.QueryString["filename"]);
            Code.Text = ReadFile(filePath);
        }
        else
        {
            FileName.Text = "Phonehub.master";
            string masterPath = Server.MapPath("../Phonehub.master");
            Code.Text = ReadFile(masterPath);
        }
      
    }

    private string ReadFile(string filepath)
    {
        string fileOutput = "";
        try
        {
            StreamReader FileReader = new StreamReader(filepath);
            //The returned value is -1 if no more characters are 
            //currently available.
            while (FileReader.Peek() > -1)
            {
                //ReadLine() Reads a line of characters from the 
                //current stream and returns the data as a string.
                fileOutput += FileReader.ReadLine().Replace("<", "&lt;").
                Replace("  ", "&nbsp;&nbsp;&nbsp;&nbsp;")
                                      + "<br />";
            }
            FileReader.Close();
        }
        catch (FileNotFoundException e)
        {
            fileOutput = e.Message;
        }
        return fileOutput;
    }
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Source Code</title>
    <link href="~/Ass2/Styles/main.css" rel="stylesheet" type="text/css" />
</head>
<body id="codePage">
    <div>
        <h1 id="codePageHeader">Source Code</h1>
        <asp:label id="FileName" CssClass="codeheader" Runat="server"/>
        <asp:label id="xmlFile" CssClass="codeheader" Runat="server"/>
        <asp:Panel id="pnlCode" CssClass="code" runat="server" Width="80%">
            <asp:label id="Code" Runat="server" />
        </asp:Panel>
    </div>
</body>
</html>
