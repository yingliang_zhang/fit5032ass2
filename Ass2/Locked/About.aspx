﻿<%@ Page Title="About Us" Language="C#" MasterPageFile="~/Phonehub.master" StylesheetTheme="SkinFile" %>

<script runat="server">

</script>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:SiteMapPath ID="SiteMapPath1" runat="server" SiteMapProvider="MyAss1SiteMapProvider">
    </asp:SiteMapPath>
    <h1>About Us</h1>
    <hr />
    <asp:Image ID="aboutLogo" runat="server" ImageUrl="~/Images/phonehublogo.png" />

    <div id="aboutText">
    <p>
        Phonehub is a news, reviews, ideas and opinions posting and sharing website, we write articles and
        reviews about cutting edge technology that related to consumer electronics, especially mobile phones,
        mobile operating systems, cellphone companies and the science they're built upon. The content on Phonehub
        is mainly about mobile phone unboxing, guides and reviews. In order to make our content more interesting
        and alive, we make frequent use of video to supplement our text content. Our articles are written
        by professional technology blog writers, observers and tech-enthusiasts.
    </p>
    <p>
        The core of Phonehub is about experiencing the beauty and joy in technology, which includes sharing
        views and opinions with our readers.We hope our visitors can find their love for technology and lead
        the tech filled lifesytle that they want through the reviews, stories and ideas on Phonehub. We also
        hope that we can provide customers practical information and advices to help them in finding the
        technology and products that best suits their needs.
    </p>
    <a href="../DisplayCode.aspx?filename=~/Ass2/Locked/About.aspx&filename2=~/Ass2/Locked/OurHistory.aspx&filename3=~/Ass2/Locked/FAQ.aspx" target="_blank" class="blueImgBtn">
        <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/codebuttonAboutus.jpg" />
    </a>
    </div>
</asp:Content>

