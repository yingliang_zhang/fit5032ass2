﻿<%@ Page Title="Email Page" Language="C#" MasterPageFile="~/Phonehub.master" StylesheetTheme="SkinFile" %>
<%@ Import Namespace="System.Net.Mail" %>
<%@ Import Namespace="System.Net" %>
<script runat="server">
    public void SendEmail(object sender, EventArgs e)
    {
        MailMessage newMsg = new MailMessage();

        foreach (GridViewRow gvRow in gvReaders.Rows)
        {
            CheckBox cb = (CheckBox)gvRow.FindControl("chkEmail");

            if (cb != null && cb.Checked)
            {
                newMsg.To.Add(new MailAddress(gvRow.Cells[1].Text,
                  gvRow.Cells[0].Text));
            }
        }

        newMsg.From = new MailAddress("marketingnews@phonehub.com", "Phonehub");
        newMsg.Subject = txtSubject.Text;
        newMsg.Body = txtMsg.Text;

        try
        {
            SmtpClient smtp = new SmtpClient();
            smtp.Host = "smtp.monash.edu.au";
            smtp.Port = 25;
            smtp.Send(newMsg);
            lblMail.Text = "Mail Successfully Sent";
        }
        catch (Exception exc)
        {
            lblMail.Text = exc.Message;
        }
    }
</script>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:AccessDataSource ID="ReaderEmailDataSource" runat="server" 
        DataFile="~/App_Data/Reader.accdb" SelectCommand="SELECT Username, Email FROM [Reader]"></asp:AccessDataSource>
    <h1>Choose user to send emails</h1>
    <hr />
    <div id="emailGv">
    <asp:GridView ID="gvReaders" runat="server" SkinID="readerEmailGv"
        DataSourceID="ReaderEmailDataSource" AllowSorting="True">
        <Columns>
            <asp:BoundField DataField="Username" 
              HeaderText="Username" SortExpression="Username" />
            <asp:BoundField DataField="Email" HeaderText="Email" SortExpression="Email" />
             <asp:TemplateField HeaderText="Select"> 
              <ItemTemplate> 
                <asp:CheckBox runat="server" id="chkEmail" />
              </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
    </div>
    <br /><br />
    <asp:label id="lblMail" CssClass="error" runat="server" />
    <div id="emailInputs">
        <table class="emailTable">
        <tr>
          <td width="15%">From</td>
          <td>Phonehub</td>
        </tr>
        <tr>
          <td width="15%">Subject</td>
          <td>
            <asp:TextBox ID="txtSubject" Width="350" runat="server" />
          </td>
        </tr>
        <tr>
          <td>Message</td>
          <td>
            <asp:TextBox runat="server" ID="txtMsg" 
              TextMode="MultiLine" Columns="55" Rows="15" />
          </td>
        </tr>
        </table>
        <asp:Button id="SendMail" runat="server" CssClass="button" 
         OnClick="SendEmail" Text="Send Email"/>
    </div>
    <br /><br />
    <a id="emailImg" class="blueImgBtn" href="../DisplayCode.aspx?filename=~/Ass2/Locked/Email.aspx" target="_blank">
        <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/codebuttonEmail.jpg" />
     </a>
</asp:Content>

