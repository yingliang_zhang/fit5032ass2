﻿<%@ Page Language="C#" StylesheetTheme="SkinFile" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">
    protected void Page_Load()
    {
        ReaderDetailsPanel.Visible = false;
    }
    protected void showReader(object sender, GridViewCommandEventArgs e)
    {
        ReaderDetailsPanel.Visible = true;
        readerID.Text = ReaderGridView.Rows[Convert.ToInt32(e.CommandArgument)].Cells[0].Text;
        titleLabel.Text = ReaderGridView.Rows[Convert.ToInt32(e.CommandArgument)].Cells[1].Text;
        username.Text = ReaderGridView.Rows[Convert.ToInt32(e.CommandArgument)].Cells[2].Text;
        email.Text = ReaderGridView.Rows[Convert.ToInt32(e.CommandArgument)].Cells[3].Text;
        password.Text = ReaderGridView.Rows[Convert.ToInt32(e.CommandArgument)].Cells[4].Text;
        phone.Text = ReaderGridView.Rows[Convert.ToInt32(e.CommandArgument)].Cells[5].Text;
        postcode.Text = ReaderGridView.Rows[Convert.ToInt32(e.CommandArgument)].Cells[6].Text;
        url.Text = ((HyperLink)ReaderGridView.Rows[Convert.ToInt32(e.CommandArgument)].Cells[7].Controls[0]).NavigateUrl;
        stateLabel.Text = ReaderGridView.Rows[Convert.ToInt32(e.CommandArgument)].Cells[8].Text;
        notiMethod.Text = ReaderGridView.Rows[Convert.ToInt32(e.CommandArgument)].Cells[9].Text;
        whereheard.Text = ReaderGridView.Rows[Convert.ToInt32(e.CommandArgument)].Cells[10].Text;
    }
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Display Records</title>
    <link href="~/Ass2/Styles/main.css" rel="stylesheet" type="text/css" />
</head>
<body id="displayRecordPage">
    <form id="form1" runat="server">
    <h1 id="displayPageHeader">All Readers of Phonehub</h1>
    <div>
        <asp:GridView ID="ReaderGridView" runat="server" DataKeyNames="ID" DataSourceID="ReaderDataSource" 
                    SkinID="readerGv" OnRowCommand="showReader">
           <Columns>
                <asp:BoundField DataField="ID" HeaderText="ID" InsertVisible="False" 
                    ReadOnly="True" SortExpression="ID" />
                <asp:BoundField DataField="Title" HeaderText="Title" SortExpression="Title" />
                <asp:BoundField DataField="Username" HeaderText="Username" 
                    SortExpression="Username" />
                <asp:BoundField DataField="Email" HeaderText="Email" SortExpression="Email" />
                <asp:BoundField DataField="Password" HeaderText="Password" 
                    SortExpression="Password" />
                <asp:BoundField DataField="Phone" HeaderText="Phone" SortExpression="Phone" />
                <asp:BoundField DataField="Postcode" HeaderText="Postcode" 
                    SortExpression="Postcode" />
                <asp:HyperLinkField DataNavigateUrlFields="Website" DataTextField="Username" 
                    HeaderText="Website" />
                <asp:BoundField DataField="State" HeaderText="State" SortExpression="State" />
                <asp:BoundField DataField="Notification" HeaderText="Notification" 
                    SortExpression="Notification" />
                <asp:BoundField DataField="WhereHeard" HeaderText="WhereHeard" 
                    SortExpression="WhereHeard" />
                <asp:ButtonField Text="Show Reader" ButtonType="Button" ControlStyle-CssClass="button"/>
            </Columns>
        </asp:GridView>
        <asp:AccessDataSource ID="ReaderDataSource" runat="server" 
            DataFile="~/App_Data/Reader.accdb" SelectCommand="SELECT * FROM [Reader]">
        </asp:AccessDataSource>
    </div>
    <asp:Panel ID="ReaderDetailsPanel" runat="server">
    <div id="ReaderDetails">
        <table>
            <tr>
                <td><b>Reader ID: </b></td>
                <td><asp:Label ID="readerID" runat="server" Text=""></asp:Label></td>
            </tr>
            <tr>
                <td><b>Title: </b></td>
                <td><asp:Label ID="titleLabel" runat="server" Text=""></asp:Label></td>
            </tr>
            <tr>
                <td><b>Username: </b></td>
                <td><asp:Label ID="username" runat="server" Text=""></asp:Label></td>
            </tr>
            <tr>
                <td><b>Email: </b></td>
                <td><asp:Label ID="email" runat="server" Text=""></asp:Label></td>
            </tr>
            <tr>
                <td><b>Password: </b></td>
                <td><asp:Label ID="password" runat="server" Text=""></asp:Label></td>
            </tr>
            <tr>
                <td><b>Phone Number: </b></td>
                <td><asp:Label ID="phone" runat="server" Text=""></asp:Label></td>
            </tr>
            <tr>
                <td><b>Post Code: </b></td>
                <td><asp:Label ID="postcode" runat="server" Text=""></asp:Label></td>
            </tr>
            <tr>
                <td><b>Website: </b></td>
                <td><asp:Label ID="url" runat="server" Text=""></asp:Label></td>
            </tr>
            <tr>
                <td><b>State: </b></td>
                <td><asp:Label ID="stateLabel" runat="server" Text=""></asp:Label></td>
            </tr>
            <tr>
                <td><b>Notification Method: </b></td>
                <td><asp:Label ID="notiMethod" runat="server" Text=""></asp:Label></td>
            </tr>
            <tr>
                <td><b>Where Heard: </b></td>
                <td><asp:Label ID="whereheard" runat="server" Text=""></asp:Label></td>
            </tr>
        </table>
    </div>
    </asp:Panel>
    <br />
    <a id="displayImg" href="../DisplayCode.aspx?filename=~/Ass2/Locked/DisplayRecords.aspx" target="_blank">
        <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/codebuttondisplayrecords.jpg" />
     </a>
     <br /><br />
    </form>
</body>
</html>
