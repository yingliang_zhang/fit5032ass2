﻿<%@ Page Language="C#" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ import Namespace="System.Data" %>
<%@ import Namespace="System.Data.OleDb" %>
<script runat="server">
    protected void Page_Load()
    {
        readerResult.Visible = false;
    }
    protected void searchReader(object sender, EventArgs e)
    {
        Button btn = (Button)sender;
        string strResultsHolder = "";
        string strConnection = "Provider = Microsoft.ACE.OLEDB.12.0; Data Source = " + Server.MapPath("~/App_Data/Reader.accdb") + ";";
        OleDbConnection objConnection = new OleDbConnection(strConnection);
        OleDbDataReader objDataReader = null;
        objConnection.Open();
        switch (btn.CommandName)
        {
            case "ID":
                string strSQL = "SELECT * FROM Reader WHERE ID LIKE '%" + readerId.Text.Trim(' ') + "%'";
                OleDbCommand objCommand = new OleDbCommand(strSQL, objConnection);

                objDataReader = objCommand.ExecuteReader();
                while (objDataReader.Read() == true)
                {
                    strResultsHolder += "<b>Reader ID:</b>" + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                        + objDataReader[0].ToString() + "<br />";
                    strResultsHolder += "<b>Title:</b>" + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                        + objDataReader[1].ToString() + "<br />";
                    strResultsHolder += "<b>Username:</b>" + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                        + objDataReader[2].ToString() + "<br />";
                    strResultsHolder += "<b>Email:</b>" + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                        + objDataReader[3].ToString() + "<br />";
                    strResultsHolder += "<b>Password:</b>" + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                        + objDataReader[4].ToString() + "<br />";
                    strResultsHolder += "<b>Phone Number:</b>" + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                        + objDataReader[5].ToString() + "<br />";
                    strResultsHolder += "<b>Postcode:</b>" + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                        + objDataReader[6].ToString() + "<br />";
                    strResultsHolder += "<b>Website:</b>" + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                        + objDataReader[7].ToString() + "<br />";
                    strResultsHolder += "<b>State:</b>" + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                        + objDataReader[8].ToString() + "<br />";
                    strResultsHolder += "<b>Notification Method: </b>" + "&nbsp;"
                        + objDataReader[9].ToString() + "<br />";
                    strResultsHolder += "<b>Where Heard:</b>" + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                        + objDataReader[10].ToString() + "<br />";
                }
                break;
            case "Username":
                strSQL = "SELECT * FROM Reader WHERE Username LIKE '%" + usernameTitle.Text.Trim(' ') + "%'";
                objCommand = new OleDbCommand(strSQL, objConnection);

                objDataReader = objCommand.ExecuteReader();
                while (objDataReader.Read() == true)
                {
                    strResultsHolder += "<b>Reader ID:</b>" + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                        + objDataReader[0].ToString() + "<br />";
                    strResultsHolder += "<b>Title:</b>" + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                        + objDataReader[1].ToString() + "<br />";
                    strResultsHolder += "<b>Username:</b>" + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                        + objDataReader[2].ToString() + "<br />";
                    strResultsHolder += "<b>Email:</b>" + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                        + objDataReader[3].ToString() + "<br />";
                    strResultsHolder += "<b>Password:</b>" + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                        + objDataReader[4].ToString() + "<br />";
                    strResultsHolder += "<b>Phone Number:</b>" + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                        + objDataReader[5].ToString() + "<br />";
                    strResultsHolder += "<b>Postcode:</b>" + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                        + objDataReader[6].ToString() + "<br />";
                    strResultsHolder += "<b>Website:</b>" + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                        + objDataReader[7].ToString() + "<br />";
                    strResultsHolder += "<b>State:</b>" + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                        + objDataReader[8].ToString() + "<br />";
                    strResultsHolder += "<b>Notification Method: </b>" + "&nbsp;"
                        + objDataReader[9].ToString() + "<br />";
                    strResultsHolder += "<b>Where Heard:</b>" + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                        + objDataReader[10].ToString() + "<br />";
                }
                break;
            case "Phone":
                strSQL = "SELECT * FROM Reader WHERE Phone LIKE '%" + phoneNum.Text.Trim(' ') + "%'";
                objCommand = new OleDbCommand(strSQL, objConnection);

                objDataReader = objCommand.ExecuteReader();
                while (objDataReader.Read() == true)
                {
                    strResultsHolder += "<b>Reader ID:</b>" + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" 
                        + objDataReader[0].ToString() + "<br />";
                    strResultsHolder += "<b>Title:</b>" + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" 
                        + objDataReader[1].ToString() + "<br />";
                    strResultsHolder += "<b>Username:</b>" + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" 
                        + objDataReader[2].ToString() + "<br />";
                    strResultsHolder += "<b>Email:</b>" + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" 
                        + objDataReader[3].ToString() + "<br />";
                    strResultsHolder += "<b>Password:</b>" + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" 
                        + objDataReader[4].ToString() + "<br />";
                    strResultsHolder += "<b>Phone Number:</b>" + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" 
                        + objDataReader[5].ToString() + "<br />";
                    strResultsHolder += "<b>Postcode:</b>" + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" 
                        + objDataReader[6].ToString() + "<br />";
                    strResultsHolder += "<b>Website:</b>" + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" 
                        + objDataReader[7].ToString() + "<br />";
                    strResultsHolder += "<b>State:</b>" + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" 
                        + objDataReader[8].ToString() + "<br />";
                    strResultsHolder += "<b>Notification Method: </b>" + "&nbsp;" 
                        + objDataReader[9].ToString() + "<br />";
                    strResultsHolder += "<b>Where Heard:</b>" + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" 
                        + objDataReader[10].ToString() + "<br />";
                }
                break;
        }
        

        objDataReader.Close();
        objConnection.Close();

        if (strResultsHolder == "")
        {
            strResultsHolder = "<b>Reader not found!</b>";
        }
        readerText.Text = strResultsHolder;
        readerResult.Visible = true;

    }
</script>

<asp:accessdatasource runat="server" DataFile="~/App_Data/Reader.accdb" id="ReaderDataSource">
</asp:accessdatasource>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Search Records</title>
    <link href="~/Ass2/Styles/main.css" rel="stylesheet" type="text/css" />
</head>
<body id="SearchRecordsPage">
    <form id="form1" runat="server">
    <h1 id="searchPageHeader">Search for a reader</h1>
    <div id="searchInputAera">
    <table>
    <tr>
      <td colspan="2"> Please enter reader id:<br/> 
      </td>
    </tr>
    <tr>
      <td class="text">   
        <asp:TextBox id="readerId" runat="server"></asp:TextBox>
      </td>
      <td>
        <asp:Button id="btnID" onclick="searchReader" 
          runat="server" 
          Text="Find by id"
          CssClass="button" CommandName="ID" />
        </td>
    </tr>
    <tr>
      <td><b>or</b></td>
    </tr>
    <tr>
      <td colspan="2">Please enter reader's username:<br/>
      </td>    
    </tr>
    <tr>
      <td>
        <asp:TextBox id="usernameTitle" runat="server"></asp:TextBox>
      </td>
      <td>
        <asp:Button id="btnUsername" onclick="searchReader" 
            runat="server" 
            Text="Find by username"
            CssClass="button" CommandName="Username" />
        </td>
     </tr>
    <tr>
      <td><b>or</b></td>
    </tr>
    <tr>
      <td colspan="2">Please enter reader's phone number:<br/> 
      </td>
    </tr>
    <tr>
      <td>   
        <asp:TextBox id="phoneNum" runat="server"></asp:TextBox>
      </td>
      <td>
        <asp:Button id="btnPhone" onclick="searchReader" 
          runat="server" 
          Text="Find by phone" 
          CssClass="button" CommandName="Phone"/>
      </td>
    </tr>
    </table>
    <asp:Panel ID="readerResult" runat="server">
        <asp:Label ID="readerText" runat="server" Text=""></asp:Label>
    </asp:Panel>
    <br />
     <a id="searchImg" href="../DisplayCode.aspx?filename=~/Ass2/Locked/SearchRecords.aspx" target="_blank">
        <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/codebuttonsearchrecords.jpg" />
     </a>
     <br /><br /><br />
    </div>
    </form>
</body>
</html>
