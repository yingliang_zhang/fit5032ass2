﻿<%@ Page Title="History" Language="C#" MasterPageFile="~/Phonehub.master" StylesheetTheme="SkinFile" %>

<script runat="server">

</script>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:SiteMapPath ID="SiteMapPath1" runat="server" SiteMapProvider="MyAss1SiteMapProvider">
    </asp:SiteMapPath>
    <h1>Our History</h1>
    <hr />
    <ul id="historyList">
        <li>In July of 2006, Phonehub was created as a technical blog by two college students who love technology
            and mobile phones and shared their reviews online.</li>
        <li>In April of 2008, Phonehub first launched as a website by the original authors and a team contained with
            five technology lovers.</li>
        <li>In April of 2009, Phonehub reached 30,000 visitors per day and celebrated its one-year anniversary.</li>
        <li>In September of 2010, Phonehub received official products review invitation from HTC.</li>
        <li>In March of 2011, Phonehub had posted 300 reviews and 60,000 members.</li>
        <li>In April of 2013, Phonehub had recived official products review invitation from Samsung, LG, Sony, Motorola
            and Apple, the website was rebulit to meet increasing visits.</li>
        <li>Now, Phonehub has become a well known mobile phone review and opinion sharing website, and we will continue
            to provide high-quality articles and videos to our readers.</li>
    </ul>
</asp:Content>

