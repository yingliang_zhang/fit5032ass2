﻿<%@ Page Title="Registration" Language="C#" MasterPageFile="~/Phonehub.master" StylesheetTheme="SkinFile" %>

<script runat="server">
    protected void Page_Load()
    {
        ResultPanel.Visible = false;
    }

    protected void SubmitClick(object sender, EventArgs e)
    {
        if (!(Page.IsValid))
        {
            return;
        }
        else
        {
            ResultPanel.Visible = true;
        }
        
        TitleLabel.Text = titleList.SelectedItem.Value;
        UsernameLabel.Text = UsernameBox.Text;
        EmailLabel.Text = EmailBox.Text;
        PasswdLabel.Text = PasswdBox.Text;
        PhoneNumberLabel.Text = PhoneBox.Text;
        PostcodeLabel.Text = PostcodeBox.Text;
        StateLabel.Text = StateDropDownList.SelectedItem.Value;
        UrlLabel.Text = UrlTextBox.Text;
        UrlLabel.NavigateUrl = UrlTextBox.Text;
        NewsMethodLabel.Text = NewsMethodListBox.SelectedItem.Value;
        if (InternetCheckBox.Checked)
        {
            InternetLabel.Text = InternetCheckBox.Text;
        }
        else
        {
            InternetLabel.Text = "";
        }
        if (TVCheckBox.Checked)
        {
            TVLabel.Text = TVCheckBox.Text;
        }
        else
        {
            TVLabel.Text = "";
        }
        if (NewspaperCheckBox.Checked)
        {
            NewspaperLabel.Text = NewspaperCheckBox.Text;
        }
        else
        {
            NewspaperLabel.Text = "";
        }
        if (FriendCheckBox.Checked)
        {
            FriendLabel.Text = FriendCheckBox.Text;
        }
        else
        {
            FriendLabel.Text = "";
        }
        whereHeardValue.Text = InternetLabel.Text + TVLabel.Text + NewspaperLabel.Text + FriendLabel.Text;
        
        ReaderDataSource.InsertParameters[0].DefaultValue = TitleLabel.Text;
        ReaderDataSource.InsertParameters[1].DefaultValue = UsernameLabel.Text;
        ReaderDataSource.InsertParameters[2].DefaultValue = EmailLabel.Text;
        ReaderDataSource.InsertParameters[3].DefaultValue = PasswdLabel.Text;
        ReaderDataSource.InsertParameters[4].DefaultValue = PhoneNumberLabel.Text;
        ReaderDataSource.InsertParameters[5].DefaultValue = PostcodeLabel.Text;
        ReaderDataSource.InsertParameters[6].DefaultValue = UrlLabel.Text;
        ReaderDataSource.InsertParameters[7].DefaultValue = StateLabel.Text;
        ReaderDataSource.InsertParameters[8].DefaultValue = NewsMethodLabel.Text;
        ReaderDataSource.InsertParameters[9].DefaultValue = whereHeardValue.Text;
        ReaderDataSource.Insert();
    }

    protected void username(object sender, ServerValidateEventArgs e)
    {
         if (e.Value.Length < 8 || e.Value.Length >= 16)
         {
             e.IsValid = false;
             ResultPanel.Visible = false;
         }
         else
         {
             e.IsValid = true;
         }
    }
       
</script>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:AccessDataSource ID="ReaderDataSource" runat="server" 
        DataFile="~/App_Data/Reader.accdb" 
        DeleteCommand="DELETE FROM [Reader] WHERE [ID] = ?" 
        InsertCommand="INSERT INTO [Reader] ([Title], [Username], [Email], [Password], [Phone], [Postcode], [Website], [State], [Notification], [WhereHeard]) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)" 
        SelectCommand="SELECT * FROM [Reader]" 
        UpdateCommand="UPDATE [Reader] SET [Title] = ?, [Username] = ?, [Email] = ?, [Password] = ?, [Phone] = ?, [Postcode] = ?, [Website] = ?, [State] = ?, [Notification] = ?, [WhereHeard] = ? WHERE [ID] = ?">
        <DeleteParameters>
            <asp:Parameter Name="ID" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="Title" Type="String" />
            <asp:Parameter Name="Username" Type="String" />
            <asp:Parameter Name="Email" Type="String" />
            <asp:Parameter Name="Password" Type="String" />
            <asp:Parameter Name="Phone" Type="String" />
            <asp:Parameter Name="Postcode" Type="String" />
            <asp:Parameter Name="Website" Type="String" />
            <asp:Parameter Name="State" Type="String" />
            <asp:Parameter Name="Notification" Type="String" />
            <asp:Parameter Name="WhereHeard" Type="String" />
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="Title" Type="String" />
            <asp:Parameter Name="Username" Type="String" />
            <asp:Parameter Name="Email" Type="String" />
            <asp:Parameter Name="Password" Type="String" />
            <asp:Parameter Name="Phone" Type="String" />
            <asp:Parameter Name="Postcode" Type="String" />
            <asp:Parameter Name="Website" Type="String" />
            <asp:Parameter Name="State" Type="String" />
            <asp:Parameter Name="Notification" Type="String" />
            <asp:Parameter Name="WhereHeard" Type="String" />
            <asp:Parameter Name="ID" Type="Int32" />
        </UpdateParameters>
    </asp:AccessDataSource>
    <asp:SiteMapPath ID="SiteMapPath1" runat="server" SiteMapProvider="MyAss1SiteMapProvider">
    </asp:SiteMapPath>
    <h1>
        Join Us
    </h1>
    <hr />
    <h3>
        Personal details:
    </h3>
    <!-- Registration Form -->
    <table id="registerForm" border="0" cellpadding="0" cellspacing="0">
    <tr>
            <td class="firstCol">
                Title:
            </td>
            <td>
                <asp:RadioButtonList ID="titleList" runat="server" SkinID="radio" >
                    <asp:ListItem Selected="True">Mr</asp:ListItem>
                    <asp:ListItem>Mrs</asp:ListItem>
                    <asp:ListItem>Ms</asp:ListItem>
                    <asp:ListItem>Miss</asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr>
            <td class="firstCol">
                Username:
            </td>
            <td>
                <asp:TextBox ID="UsernameBox" runat="server"></asp:TextBox> 
            </td>
            <td class="validatorCol">
                <asp:CustomValidator ID="UsernameValidator" runat="server" ControlToValidate="UsernameBox" Display="Dynamic"
                    OnServerValidate="username" ErrorMessage="A username must be between 8 and 16 characters!">
                </asp:CustomValidator>
                <asp:RequiredFieldValidator ID="UsernameRequiredFieldValidator" ControlToValidate="UsernameBox" runat="server" 
                    Display="Dynamic" ErrorMessage="Username can not be blank!">
                </asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="firstCol">
                Email Address:
            </td>
            <td>
                <asp:TextBox ID="EmailBox" runat="server"></asp:TextBox>
            </td>
            <td class="validatorCol">
                <asp:RegularExpressionValidator ID="EmailValidator" runat="server" Display="Dynamic"
                    ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="EmailBox" 
                    ErrorMessage="Invalid Email Address!">
                </asp:RegularExpressionValidator>
                <asp:RequiredFieldValidator ID="EmailRequiredFieldValidator" runat="server" ControlToValidate="EmailBox" 
                    Display="Dynamic" ErrorMessage="Please enter a valid email address."></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="firstCol">
                Password:
            </td>
            <td>
                <asp:TextBox ID="PasswdBox" runat="server" TextMode="Password"></asp:TextBox>
            </td>
            <td class="validatorCol">
                <asp:RegularExpressionValidator ID="PasswdLengthValidator" runat="server" ControlToValidate="PasswdBox"
                    Display="Dynamic" ErrorMessage="Please enter at least 6 characters." ValidationExpression=".{6}.*">
                </asp:RegularExpressionValidator>
                <asp:RequiredFieldValidator ID="PasswordRequiredFieldValidator" runat="server" ControlToValidate="PasswdBox" 
                    Display="Dynamic" ErrorMessage="Please enter a password."></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="firstCol">
                Confirm Password:
            </td>
            <td>
                <asp:TextBox ID="RePasswdBox" runat="server" TextMode="Password"></asp:TextBox>
            </td>
            <td class="validatorCol">
                <asp:CompareValidator ID="PasswordCompareValidator" runat="server" 
                    ErrorMessage="Your passwords do not match." ControlToValidate="PasswdBox" 
                    ControlToCompare="RePasswdBox" Display="Dynamic" >
                </asp:CompareValidator>
                <asp:RequiredFieldValidator ID="RepasswdRequiredFieldValidator" runat="server" ControlToValidate="RePasswdBox" 
                   Display="Dynamic" ErrorMessage="Please re-enter the password.">
                </asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="firstCol">
                Phone Number:
            </td>
            <td>
                <asp:TextBox ID="PhoneBox" runat="server"></asp:TextBox>
            </td>
            <td class="validatorCol">
                <asp:RegularExpressionValidator ID="PhoneExpressionValidator" runat="server" ControlToValidate="PhoneBox"
                    ValidationExpression="^(\(?\+?[0-9]*\)?)?[0-9_\- \(\)]*$" Display="Dynamic"
                    ErrorMessage="Enter a valid phone number">
                </asp:RegularExpressionValidator>
                <asp:RequiredFieldValidator ID="PhonenumberRequiredFieldValidator" runat="server" ControlToValidate="PhoneBox" 
                   Display="Dynamic" ErrorMessage="Please enter a phone number.">
                </asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="firstCol">
                Post Code:
            </td>
            <td>
                <asp:TextBox ID="PostcodeBox" runat="server"></asp:TextBox>
            </td>
            <td class="validatorCol">
                <asp:RegularExpressionValidator ID="PostcodeExpressionValidator" runat="server" 
                    ControlToValidate="PostcodeBox" ValidationExpression="\d{4}"
                    ErrorMessage="Post Code must be 4 numeric digits!" Display="Dynamic"></asp:RegularExpressionValidator>
                <asp:RangeValidator ID="PostcodeRangeValidator" runat="server" ControlToValidate="PostcodeBox" 
                    ErrorMessage="Australia postcodes must be between 0800 and 7999!" 
                    Display="Dynamic" MaximumValue="7999" MinimumValue="0800" Type="Integer"></asp:RangeValidator>
                <asp:RequiredFieldValidator ID="PostcodeRequiredFieldValidator1" runat="server" ControlToValidate="PostcodeBox" 
                   Display="Dynamic" ErrorMessage="Please enter a postcode.">
                </asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="firstCol">
                Your Website:
            </td>
            <td>
                <asp:TextBox ID="UrlTextBox" runat="server"></asp:TextBox>
            </td>
            <td class="validatorCol">
                <asp:RegularExpressionValidator ID="UrlExpressionValidator" runat="server"  ControlToValidate="UrlTextBox" 
                 ValidationExpression="^(ht|f)tp(s?)\:\/\/[0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*(:(0-9)*)*(\/?)([a-zA-Z0-9\-\.\?\,\'\/\\\+&amp;%\$#_]*)?$"   
                 Display="Dynamic"  ErrorMessage="Please enter a valid url. e.g.: http://www.google.com">
                </asp:RegularExpressionValidator>
                <asp:RequiredFieldValidator ID="UrlRequiredFieldValidator" runat="server" ControlToValidate="UrlTextBox" 
                   Display="Dynamic" ErrorMessage="Please enter a valid URL. e.g.: http://www.google.com">
                </asp:RequiredFieldValidator>
            </td>
        </tr>

        <tr>
            <td class="firstCol">
                State:
            </td>
            <td>
                <asp:DropDownList ID="StateDropDownList" runat="server">
                    <asp:ListItem>ACT</asp:ListItem>
                    <asp:ListItem>New South Wales</asp:ListItem>
                    <asp:ListItem>Northern Territory</asp:ListItem>
                    <asp:ListItem>Queensland</asp:ListItem>
                    <asp:ListItem>South Australia</asp:ListItem>
                    <asp:ListItem>Tasmania</asp:ListItem>
                    <asp:ListItem>Victoria</asp:ListItem>
                    <asp:ListItem>Western Australia</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="firstCol">
                News Notification:
            </td>
            <td>
                <asp:ListBox ID="NewsMethodListBox" runat="server" Rows="3">
                    <asp:ListItem Selected="True">No News for Me</asp:ListItem>
                    <asp:ListItem>Send to My Account</asp:ListItem>
                    <asp:ListItem>Send by Mail</asp:ListItem>
                    <asp:ListItem>Send to Email</asp:ListItem>
                    <asp:ListItem>Email and Mail</asp:ListItem>
                </asp:ListBox>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                Where did you hear about us?
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:CheckBox ID="InternetCheckBox" runat="server" Text="Internet" />
                <asp:CheckBox ID="TVCheckBox" runat="server" Text="TV" />
                <asp:CheckBox ID="NewspaperCheckBox" runat="server" Text="Newspaper" />
                <asp:CheckBox ID="FriendCheckBox" runat="server" Text="Friends/Family" />
            </td>
        </tr>
    </table>
    <asp:Button ID="JoinButton" runat="server" Text="Join"  
        OnClick="SubmitClick" BorderStyle="Outset" CssClass="button" />
    <!-- Result Panel -->
    <asp:Panel ID="ResultPanel" runat="server">
        <h3>
            Registerd Details:
        </h3>
        <table id="result" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td class="resultFirstCol">
                    Title:
                </td>
                <td>
                    <asp:Label ID="TitleLabel" runat="server" Text=""></asp:Label>
                </td>
                <td class="resultFirstCol">
                    Username:
                </td>
                <td>
                    <asp:Label ID="UsernameLabel" runat="server" Text=""></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="resultFirstCol">
                    Email:
                </td>
                <td>
                    <asp:Label ID="EmailLabel" runat="server" Text=""></asp:Label>
                </td>
                <td class="resultFirstCol">
                    Password:
                </td>
                <td>
                    <asp:Label ID="PasswdLabel" runat="server" Text=""></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="resultFirstCol">
                    Phone:
                </td>
                <td>
                    <asp:Label ID="PhoneNumberLabel" runat="server" Text=""></asp:Label>
                </td>
                <td class="resultFirstCol">
                    Post Code:
                </td>
                <td>
                    <asp:Label ID="PostcodeLabel" runat="server" Text=""></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="resultFirstCol">
                    State:
                </td>
                <td>
                    <asp:Label ID="StateLabel" runat="server" Text=""></asp:Label>
                </td>
                <td class="resultFirstCol">
                    News Notification:
                </td>
                <td>
                    <asp:Label ID="NewsMethodLabel" runat="server" Text=""></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="resultFirstCol">
                   Your Website:
                </td>
                <td>
                    <asp:HyperLink ID="UrlLabel" runat="server"></asp:HyperLink>
                </td>
                <td class="resultFirstCol">
                    Where Heard:
                </td>
                <td>
                    <asp:Label ID="InternetLabel" runat="server" Text=""></asp:Label>
                    <asp:Label ID="TVLabel" runat="server" Text=""></asp:Label>
                    <asp:Label ID="NewspaperLabel" runat="server" Text=""></asp:Label>
                    <asp:Label ID="FriendLabel" runat="server" Text=""></asp:Label>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <br />
    <a href="DisplayRecords.aspx" target="_blank">Click here to see all the readers</a>&nbsp;&nbsp;&nbsp;
    <a href="SearchRecords.aspx" target="_blank">Click here to search for a reader</a><br />
    <br />
    <a class="blueImgBtn" href="../DisplayCode.aspx?filename=~/Ass2/Locked/Registration.aspx" target="_blank">
        <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/codebuttonAddRecords.jpg" />
     </a>
    <asp:Label ID="whereHeardValue" runat="server" Text="" CssClass="whereHeardValueLabel"></asp:Label>
</asp:Content>



